<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;
use File;

class CastController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        // $cast = DB::table('cast')->get();
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }
    
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required|numeric',
            'photo' => 'required|mimes:jpeg,jpg,png|max:2000',
            'bio' => 'required',
        ]);
        // $query = DB::table('cast')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        // $cast = new Cast;
        // $cast->nama = $request["nama"];
        // $cast->umur = $request["umur"];
        // $cast->bio = $request["bio"];
        // $cast->save();
        $photo = $request->photo;
        $new_photo = time() . ' - ' . $photo->getClientOriginalName();
        $request->photo->move(public_path('photo'), $new_photo);
        $cast = Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "photo" => $new_photo,
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('success', 'Data berhasil disimpan!');
    }

    public function show($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            
            'bio' => 'required',
        ]);

        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        //     ]);

        $cast = Cast::findorfail($id);

        if($request->has('photo')){
            $path = 'photo/';
            File::delete($path . $cast->photo);
            $foto = $request->photo;
            $new_foto = time() . ' - ' . $foto->getClientOriginalName();
            $foto->move($path, $new_foto);
            $cast_data = [
                "nama" => $request->nama,
                "umur" => $request->umur,
                "photo" => $new_foto,
                "bio" => $request->bio,
            ];
        } else {
            $cast_data = [
                "nama" => $request->nama,
                "umur" => $request->umur,
                "bio" => $request->bio,
            ];
        }
        $cast->update($cast_data);
        
        // $cast = Cast::where('id', $id)
        //             ->update([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        return redirect('/cast')->with('success', 'Data berhasil diedit!');
    }

    public function destroy($id)
    {
        // $query = DB::table('cast')->where('id', $id)->delete();

        // Cast::destroy($id);

        $cast = Cast::findorfail($id);
        $cast->delete();

        $path = "photo/";
        File::delete($path . $cast->photo);
        return redirect('/cast')->with('success', 'Data berhasil dihapus!');
    }


}

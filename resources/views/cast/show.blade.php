@extends('layout.master')

@section('judul')

Detail Data Pemain Film
    
@endsection

@section('content')
<div class="card" style="width:18rem;">
    <img class="card-img-top" src="{{ asset('photo/'.$cast->photo) }}" alt="{{$cast->nama}}">
    <div class="card-body">
      <h5 class="card-title">{{$cast->nama}}</h5>
      <p class="card-text">{{$cast->bio}}</p>
      <a href="#" class="btn btn-primary">Read More</a>
    </div>
</div>
@endsection
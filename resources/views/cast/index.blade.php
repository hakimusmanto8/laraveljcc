@extends('layout.master')

@section('judul')

Halaman List Cast
    
@endsection

@section('content')
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>    
@endif
<a href="{{ route('cast.create') }}" class="btn btn-primary my-2">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{Str::limit($value->bio, 50)}}</td>
                        <td>
                            <div class="form-row align-items-center">
                            <a href="{{ route('cast.show', ['cast' => $value->id]) }}" class="btn btn-info mr-1">Show</a>
                            <a href=" {{ route('cast.edit', ['cast' => $value->id]) }} " class="btn btn-primary mr-1">Edit</a>
                            <form action="{{ route('cast.destroy', ['cast' => $value->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
@csrf
<p>First name:<br/>
<input type="text" name="namadepan"><br/>
<p>Last name:<br/>
<input type="text" name="namabelakang"><br/>
<p></p>Gender:<br/>
<input type="radio" name="gender" value="Male"/>Male<br/>
<input type="radio" name="gender" value="Female"/>Female<br/>
<input type="radio" name="gender" value="Other"/>Other<br/>
<p>Nationality:<br/>
<select name="nationality">
    <option value="indonesian" name="nationality">Indonesian</option>
    <option value="singaporian" name="nationality">Singaporian</option>
    <option value="malaysian" name="nationality">Malaysian</option>
    <option value="australian" name="nationality">Australian</option>
</select><br/>
<p>Languange Spoken:<br/>
<input type="checkbox" name="bahasa" value="bahasa indonesia"/>Bahasa Indonesia<br/>
<input type="checkbox" name="bahasa" value="english"/>English<br/>
<input type="checkbox" name="bahasa" value="other"/>Other<br/>
<p>Bio:</br>
<textarea cols="50" rows="20"></textarea><br/>
<input type="submit" value="Sign Up">
</form>
</body>
</html>